package com.sompsnt.jackson.csv.service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.sompsnt.jackson.csv.domain.Persona;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class CsvFileService {

    public File generar(List<Persona> personas) throws IOException {
        CsvMapper mapper = new CsvMapper();
        mapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);

        CsvSchema schema = CsvSchema.builder().addColumn("id").addColumn("nombre").addColumn("apellido").build();

        ObjectWriter writer = mapper.writerFor(Persona.class).with(schema);

        File tempFile = new File("output.csv");
        
        writer.writeValues(tempFile).writeAll(personas);
        return tempFile.getAbsoluteFile();
    }

}
