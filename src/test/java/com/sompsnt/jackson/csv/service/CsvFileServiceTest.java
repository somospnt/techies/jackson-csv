package com.sompsnt.jackson.csv.service;

import com.sompsnt.jackson.csv.domain.Persona;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CsvFileServiceTest {

    private CsvFileService csvFileService = new CsvFileService();

    @BeforeEach
    public void setUp() {
        csvFileService = new CsvFileService();
    }

    @Test
    public void generar_conPersonas_retornaArchivo() throws IOException {
        List<Persona> personas = Arrays.asList(new Persona(Long.MIN_VALUE, "coco, pepe", "coco"), new Persona(Long.MIN_VALUE, "pepe", "pepe"));
        File result = csvFileService.generar(personas);
        Stream<String> filas = Files.lines(result.toPath());
        assertTrue(Files.lines(result.toPath()).count() > 0);
        filas.forEach(System.out::println);
    }

}
